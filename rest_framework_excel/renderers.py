from StringIO import StringIO
from rest_framework_csv_sorted.renderers import SortedCSVRenderer
from xlwt import *


class ExcelRenderer(SortedCSVRenderer):
    """
    Renderer which serializes to Excel
    """

    media_type = 'application/ms-excel'
    format = 'xls'
    level_sep = '.'

    def render(self, data, media_type=None, renderer_context=None):
        """
        Renders serialized *data* into Excel. For a dictionary:
        """
        if data is None:
            return ''

        stream = StringIO()
        workbook = Workbook(encoding="UTF-8")
        sheet = workbook.add_sheet('ISIS')

        table = self.tablize(data)

        for rowIndex, row in enumerate(table):
            for colIndex, elem in enumerate(row):
                if isinstance(elem, basestring):
                    sheet.write(rowIndex, colIndex, elem.encode('utf-8'))
                else:
                    sheet.write(rowIndex, colIndex, elem)

        workbook.save(stream)
        return stream.getvalue()
